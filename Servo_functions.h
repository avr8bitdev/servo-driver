/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HAL_DRIVERS_SERVO_DRIVER_SERVO_FUNCTIONS_H_
#define HAL_DRIVERS_SERVO_DRIVER_SERVO_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"
#include "../../MCAL_drivers/DIO_driver/DIO_functions.h"

// Servo direction
typedef enum
{
    Servo_direction_ClockWise,
    Servo_direction_AntiClockWise
} Servo_direction_t;

// DC Servo
typedef struct
{
    DIO_port_t portServo; // Servo port
    u8 pinServo;          // Servo pin
    u8 Servo_off_state;   // Servo off state: ON, OFF
} Servo_obj_t;

typedef const Servo_obj_t* Servo_cptr_t;

void Servo_vidInit(Servo_cptr_t structPtrServoCpy);

void Servo_vidRotate(Servo_direction_t enumServoDirectionCpy, u16 u16ItrCpy, Servo_cptr_t structPtrServoCpy);

void Servo_vidRotateAt(u16 u16AngleCpy, Servo_cptr_t structPtrServoCpy);


#endif /* HAL_DRIVERS_SERVO_DRIVER_SERVO_FUNCTIONS_H_ */

