/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Servo_functions.h"
#include "../../MCAL_drivers/Timer_driver/Timer_functions.h"
#include <util/delay.h>


inline void Servo_vidInit(Servo_cptr_t structPtrServoCpy)
{
    DIO_vidSet_pinDirection(structPtrServoCpy->portServo, structPtrServoCpy->pinServo, OUTPUT);
    DIO_vidSet_pinValue(structPtrServoCpy->portServo, structPtrServoCpy->pinServo, structPtrServoCpy->Servo_off_state);
}

void Servo_vidRotate(Servo_direction_t enumServoDirectionCpy, u16 u16ItrCpy, Servo_cptr_t structPtrServoCpy)
{
    switch (enumServoDirectionCpy)
    {
        case Servo_direction_AntiClockWise:
            Servo_vidRotateAt(180, structPtrServoCpy);
        break;

        case Servo_direction_ClockWise:
            Servo_vidRotateAt(0, structPtrServoCpy);
        break;
    }
}

void Servo_vidRotateAt(u16 u16AngleCpy, Servo_cptr_t structPtrServoCpy)
{
    // -> 710
    // <- 2510
    // eqn: t (us) = 10X + 710
    // ex: 90 = 1610us

    // prescaler: 1
    // cmp val: angle

    // convert angle to time (in us)
    u16 time_us = u16AngleCpy * 10 + 710;

    // convert time (in us) to # of ticks
    u16AngleCpy = time_us * (F_CPU / 1e6);

    // setup timer
    Timer1_vidInit(Timer1_prescaler_off, Timer1_mode_CTC_TOP_OCR1A);
    Timer1_vidDisableCmpINT(Timer1_cmp_OCR1A);
    Timer1_vidSetCmpValue(u16AngleCpy - 1, Timer1_cmp_OCR1A);

    for (u8 i = 0; i < 30; i++)
    {
        // turn on Timer then Servo (to compensate for overhead of calls to timer)
        Timer1_vidSetClock(Timer1_prescaler_div_1);
        DIO_vidSet_pinValue(structPtrServoCpy->portServo, structPtrServoCpy->pinServo, !(structPtrServoCpy->Servo_off_state));

        // wait until compare-match occur
        while(!Timer1_u8isCmpMatch(Timer1_cmp_OCR1A))
        {}

        // turn off Servo
        DIO_vidSet_pinValue(structPtrServoCpy->portServo, structPtrServoCpy->pinServo, structPtrServoCpy->Servo_off_state);

        // complete pulse width
        _delay_us(20000);

        // turn off timer and clean-up
        Timer1_vidSetClock(Timer1_prescaler_off);
        Timer1_vidResetValue();
        Timer1_vidResetCmpMatchFlag(Timer1_cmp_OCR1A);
    }

    Timer1_vidResetCmpValue(Timer1_cmp_OCR1A);
}

